const blackjack = document.getElementById('blackjack');
const winner = document.getElementById('winner');
const dealButton = document.getElementById('deal');
const hitButton = document.getElementById('hit');
const standButton = document.getElementById('stand');
const replayButton = document.getElementById('replay');

const suites = ['Spades', 'Hearts', 'Clubs', 'Diamonds'];
const cardValues = [2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11]
const names = ['Two of ', 'Three of ', 'Four of ', 'Five of ', 'Six of ', 'Seven of ', 'Eight of ', 'Nine of ', 'Ten of ', 'Jack of ', 'Queen of ', 'King of ', 'Ace of ']

let deckOfCards = [];
let aceChanged;

hitButton.disabled = true;
standButton.disabled = true;
replayButton.disabled = true;

class Card {
    constructor (suite, value, name) {
        this.value = value;
        this.name = name + suite;
    }
};

class Player {
    constructor (name, id1, id2) {
        this.name = name;
        this.hand = [];
        this.score = 0;
        this.handAnchor = document.getElementById(id1);
        this.scoreAnchor = document.getElementById(id2);
    }
    totalScore() {
        this.score = this.hand.reduce((a, b) => a + b.value, 0);
    }
    joinHand() {
        this.handAnchor.innerHTML = this.hand.map(card => card.name).join(', ');
    }
    showScore() {
        this.scoreAnchor.innerHTML = this.score;
    }
    drawCard() {
        let randomNumber = Math.floor(Math.random() * deckOfCards.length);
        this.hand.push(deckOfCards[randomNumber]);
        this.totalScore();
        deckOfCards.splice(randomNumber, 1);
    }
    acesChange() {
        let total;
        aceChanged = 0;
        for (let i = 0; i < this.hand.length; i++) {
            if(this.hand[i].value == 11) {
                this.hand[i].value = 1;
            }
            total = this.hand.reduce((a, b) => a + b.value, 0);
            if (total < 22) {
                aceChanged = 1;
                break;
            }
        }
    }
    reset() {
        this.hand = [];
        this.score = 0;
        this.handAnchor.innerHTML = '';
        this.scoreAnchor.innerHTML = '';
    }
};

class Game {
    constructor() {
        this.player1 = new Player('Player1', 'player1DrawnCardsAnchor', 'player1CardsValAnchor');
        this.dealer = new Player('Dealer', 'dealerDrawnCardsAnchor', 'dealerCardsValAnchor');
    }
    
    createDeck() {
        for (let i = 0; i < suites.length; i++) {
            for (let j = 0; j < cardValues.length; j++) {
                let newCard = new Card(suites[i], cardValues[j], names[j]);
                deckOfCards.push(newCard);
            }
        }    
    }

    endOfGame(winningPlayer) {
        winner.innerHTML = winningPlayer.name + ' is the winner!';
        hitButton.disabled = true;
        standButton.disabled = true;
        replayButton.disabled = false;
        this.dealer.joinHand();
        this.dealer.showScore();
    }

    endOfGameDead(winningPlayer, losingPlayer){
        this.endOfGame(winningPlayer);
        losingPlayer.scoreAnchor.innerHTML = losingPlayer.score + 'DEAD!';
    }

    blackjackWinner(winningPlayer) {
        blackjack.innerHTML = winningPlayer.name + ' has Blackjack!';
        this.endOfGame(winningPlayer);
    }

    decideWinner() {
        if (this.dealer.score >= this.player1.score) {
            this.dealer.showScore();
            this.endOfGame(this.dealer);
        } else {
            this.dealer.showScore();
            this.endOfGame(this.player1);
        }
        if (this.player1.score == 21 && this.dealer.score !== 21) {
            this.blackjackWinner(this.player1);
        }
    }

    drawCardPlayer() {
        this.player1.drawCard();
        this.player1.joinHand();
        if (this.player1.score > 21) {
            this.player1.acesChange();
            this.player1.totalScore();
            this.player1.showScore();
            if (aceChanged === 0) {
                this.endOfGameDead(this.dealer, this.player1);
            }
        } else {
            this.player1.showScore();
        }
    }

    drawCardDealer() {
        this.dealer.drawCard();
        this.dealer.handAnchor.innerHTML = this.dealer.hand[0].name;
        this.dealer.scoreAnchor.innerHTML = this.dealer.hand[0].value;
        if (this.dealer.score == 21) {
            this.blackjackWinner(this.dealer);
            this.dealer.joinHand();
            this.dealer.showScore();
        }
        if (this.dealer.score > 21) {
            this.dealer.acesChange();
            this.dealer.totalScore();
            if (aceChanged === 0) {
                this.endOfGameDead(this.player1, this.dealer);
            }
        }
    }

    start() {
        const game = new Game();
        this.createDeck();
        for (let i = 0; i < 2; i++) {
            this.drawCardPlayer();
            this.drawCardDealer();
        }
    
        hitButton.disabled = false;
        standButton.disabled = false;
        dealButton.disabled = true;
    }

    dealersTurn() {
        hit.disabled = true;
        stand.disabled = true;
        if (this.player1.score == 16) {
            while (this.dealer.score < 16) {
                this.drawCardDealer();
            }
        } else {
            while (this.dealer.score < 17) {
                this.drawCardDealer();
            }
        }
        if (this.dealer.score < 22) {
            this.dealer.showScore();
        }
        this.decideWinner();
        this.dealer.joinHand();
    }
    
    playAgain() {
        dealButton.disabled = false;
        replayButton.disabled = true;
    
        blackjack.innerHTML = '';
        winner.innerHTML = '';
        deckOfCards = [];

        this.player1.reset();
        this.dealer.reset();
    }
};

const game = new Game();

dealButton.addEventListener('click', () => game.start());
hitButton.addEventListener('click', () => game.drawCardPlayer());
standButton.addEventListener('click', () => game.dealersTurn());
replayButton.addEventListener('click', () => game.playAgain());


